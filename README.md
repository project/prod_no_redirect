# Prod Should not Redirect to Installer

The Prod Should Not Redirect to Installer module is designed to prevent 
production environments from accidentally redirecting users to the Drupal 
installation script. This can happen in cases where the database connection 
is temporarily lost or the settings.php file is not properly configured. 
By intercepting these redirects, the module ensures that your 
production site remains accessible and presents a more 
professional error message or page instead of risking exposure 
of sensitive configuration interfaces.

For a full description of the module, visit the
[project page](https://drupal.org/project/prod_no_redirect).

Submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/issues/prod_no_redirect).


## Table of contents

- Requirements
- Installation
- Configuration
- Maintainers


## Requirements

This module requires no modules outside of Drupal core.


## Installation

Install as you would normally install a contributed Drupal module. For further 
information, see 
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).


## Configuration

After installation, no further configuration is necessary. 
The module operates automatically to prevent redirection to 
the installer in production environments.


## Maintainers

- [Moshe Weitzman](https://www.drupal.org/u/moshe-weitzman)
