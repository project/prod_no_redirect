<?php

namespace Drupal\prod_no_redirect\EventSubscriber;

use Drupal\Core\Database\Connection;
use Drupal\Core\EventSubscriber\ExceptionDetectNeedsInstallSubscriber as CoreExceptionDetectNeedsInstallSubscriber;

/**
 * Event subscriber that determines whether an exception triggers a redirect.
 */
class ExceptionDetectNeedsInstallSubscriber extends CoreExceptionDetectNeedsInstallSubscriber {

  /**
   * Determines whether an exception triggers a redirect to the installer.
   *
   * @param \Throwable $exception
   *   The exception to check.
   * @param \Drupal\Core\Database\Connection|null $connection
   *   (optional) The database connection. Defaults to NULL.
   *
   * @return bool
   *   TRUE if the exception should trigger a redirect to installer, FALSE
   *   otherwise.
   */
  protected function shouldRedirectToInstaller(\Throwable $exception, ?Connection $connection = NULL) {
    return FALSE;
  }

}
