<?php

namespace Drupal\prod_no_redirect;

use Drupal\Core\Database\Connection;
use Drupal\Core\DrupalKernel;

/**
 * Custom Drupal kernel to handle redirections to the installer.
 */
class ProdNoRedirectDrupalKernel extends DrupalKernel {

  /**
   * Determines whether an exception triggers a redirect to the installer.
   *
   * @param \Throwable $exception
   *   The exception to check.
   * @param \Drupal\Core\Database\Connection|null $connection
   *   (optional) The database connection.
   *
   * @return bool
   *   TRUE if the exception should trigger a redirect to installer, FALSE
   *   otherwise.
   */
  protected function shouldRedirectToInstaller(\Throwable $exception, ?Connection $connection = NULL) {
    return FALSE;
  }

}
